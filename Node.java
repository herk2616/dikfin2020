class Node{
	// Variables
	String name;
	int index;
	boolean passed;
	Node nnode1;
	int ndist1;
	Node nnode2;
	int ndist2;
	Node nnode3;
	int ndist3;
	Node nnode4;
	int ndist4;

	// Constructor
	public Node(String name, int index){
		this.name = name;
		this.index = index;
		this.passed = false;
		this.nnode1 = null;
		this.nnode2 = null;
		this.nnode3 = null;
		this.nnode4 = null;
		this.ndist1 = 0;
		this.ndist2 = 0;
		this.ndist3 = 0;
		this.ndist4 = 0;
	}

	// Change state to passed
	public void pass(){
		this.passed = true;
	}

	// Set near nodes
	public void setNn1(Node nn1, int nd1){
		this.nnode1 = nn1;
		this.ndist1 = nd1;
	}	// nextnode1 Setter

	public void setNn2(Node nn2, int nd2){
		this.nnode2 = nn2;
		this.ndist2 = nd2;
	}	// nextnode1 Setter

	public void setNn3(Node nn3, int nd3){
		this.nnode3 = nn3;
		this.ndist3 = nd3;
	}	// nextnode1 Setter

	public void setNn4(Node nn4, int nd4){
		this.nnode4 = nn4;
		this.ndist4 = nd4;
	}	// nextnode1 Setter


	//get all nodes
	public Node[] getNodes(){
		Node[] n = new Node[5];
		n[0] = null;
		n[1] = this.nnode1;
		n[2] = this.nnode2;
		n[3] = this.nnode3;
		n[4] = this.nnode4;
		return n;
	}

	// get i node
	public Node getNn(int i){
		switch (i) {
			case 1: return this.nnode1;
			case 2: return this.nnode2;
			case 3: return this.nnode3;
			case 4: return this.nnode4;
		}
		return null;
	}	// nnode Getter

	//get i nodes.name
	public String getNs(int i){
		switch (i) {
			case 1:
				if(nnode1 != null ){
					return  nnode1.name;
				}else{
					return "";
				}
			case 2:
				if(nnode2 != null ){
					return  nnode2.name;
				}else{
					return "";
				}
			case 3:
				if(nnode3 != null ){
					return  nnode3.name;
				}else{
					return "";
				}
			case 4:
				if(nnode4 != null ){
					return  nnode4.name;
				}else{
					return "";
				}
		}
		return "";
	}

	// get i nodes.distance
	public int getNd(int i){
		switch (i) {
			case 1:
				if(nnode1 != null ){
					return  ndist1;
				}else{
					return 999;
				}
			case 2:
				if(nnode2 != null ){
					return  ndist2;
				}else{
					return 999;
				}
			case 3:
				if(nnode3 != null ){
					return  ndist3;
				}else{
					return 999;
				}
			case 4:
				if(nnode4 != null ){
					return  ndist4;
				}else{
					return 999;
				}
		}
		return 0;
	}

}	// End of class
