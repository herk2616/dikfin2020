---
title: "Net final exercise"
author: "Irakleios Kopitas"
date: "29/jun/2020"
geometry: margin=3cm
output: pdf_document
header-includes:
- \setlength{\parindent}{1em}
- \setlength{\parskip}{0em}
---

![Unipi](http://www.unipi.gr/unipi/odigos_spoudon/unipilogo.png)


\pagebreak

# Mορφή εισόδου.

Η μορφή εισόδου που επέλεξα είναι σε ένα αρχείο ή κάθε σειρά αναφέρετε σε ένα
κόμβο, πχ. η σειρά 1 αναφέρετε στον 1ο κόμβο. Μέσα στις σειρές υπάρχουν 8 ορίσματα
χωρισμένα με κόμμα και τα ορίσματα αυτά είναι οι γειτονικοί κόμβοι και οι αντίστοιχες
καθυστερήσεις τους.

\

Για την τοπολογία :

\

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```C
                                       -{3}-
                            -{2}- [n2]         [n3] -{12}-
                       [n1]                               [n4]
                               -{50}-           -{10}-
                                        [n5]

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Η αναπαράσταση στο αρχείο είναι :

\

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```C
n2,2,n5,50,null,0,null,0
n1,2,n3,3,null,0,null,0
n2,3,n4,12,null,0,null,0
n3,12,n5,10,null,0,null,0
n1,50,n4,10,null,0,null,0
null,0,null,0,null,0,null,0
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Για την τοπολογία του σχήματος τής εργασίας η αναπαράσταση είναι :

\


```C
n2,8,n4,1,null,0,null,0
n1,8,n4,5,n5,1,n3,4
n2,4,n4,1,n5,1,n6,1
n1,1,n2,5,n3,1,n5,1
n4,1,n2,1,n3,1,n6,4
n5,4,n3,1,null,0,null,0
```

\pagebreak

# Δομές για την εσωτερική αναπαράσταση της εισόδου.

Η δομή του κόμβου είναι ένα αντικείμενο κόμβος πού έχει:

\

* Ένα String name με το όνομα του κόμβου.
* Ένα int index πού είναι η θέση στον πίνακα.
* Ένα boolean passed όπου ορίζει αν έχει περαστεί ο κόμβος.
* Ένα Node nnode1 που είναι ο γειτονικός κόμβος 1.
* Ένα int ndist1; όπου είναι η απόσταση από τον κόμβο 1.
* Ένα Node nnode2 που είναι ο γειτονικός κόμβος 2.
* Ένα int ndist2; όπου είναι η απόσταση από τον κόμβο 2.
* Ένα Node nnode3 που είναι ο γειτονικός κόμβος 3.
* Ένα int ndist3; όπου είναι η απόσταση από τον κόμβο 3.
* Ένα Node nnode4 που είναι ο γειτονικός κόμβος 4.
* Ένα int ndist4; όπου είναι η απόσταση από τον κόμβο 4.

\

Και ο κώδικας της κλάσης node :

\

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
class Node{
	// Variables
	String name;
	int index;
	boolean passed;
	Node nnode1;
	int ndist1;
	Node nnode2;
	int ndist2;
	Node nnode3;
	int ndist3;
	Node nnode4;
	int ndist4;

	// Constructor
	public Node(String name, int index){
		this.name = name;
		this.index = index;
		this.passed = false;
		this.nnode1 = null;
		this.nnode2 = null;
		this.nnode3 = null;
		this.nnode4 = null;
		this.ndist1 = 0;
		this.ndist2 = 0;
		this.ndist3 = 0;
		this.ndist4 = 0;
	}

	// Change state to passed
	public void pass(){
		this.passed = true;
	}

	// Set near nodes
	public void setNn1(Node nn1, int nd1){
		this.nnode1 = nn1;
		this.ndist1 = nd1;
	}	// nextnode1 Setter

	public void setNn2(Node nn2, int nd2){
		this.nnode2 = nn2;
		this.ndist2 = nd2;
	}	// nextnode1 Setter

	public void setNn3(Node nn3, int nd3){
		this.nnode3 = nn3;
		this.ndist3 = nd3;
	}	// nextnode1 Setter

	public void setNn4(Node nn4, int nd4){
		this.nnode4 = nn4;
		this.ndist4 = nd4;
	}	// nextnode1 Setter


}	// End of class

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak


# Ο τρόπος με τον οποίο ο χρήστης θα επιλέγει τον κόμβο – αφετηρία των αλγορίθμων  δρομολόγησης.

Ο τρόπος με τον οποίο ο χρήστης θα επιλέγει τον κόμβο είναι με ερώτημα πριν
την εκτέλεση του προγράμματος. Συγκεκριμένα :

\
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
// Ask user for node
System.out.println("Select Node :\n");
for (int i=1; i<=6; i++)
	System.out.println(n[i].name);
Scanner sc = new Scanner(System.in);
input_node = sc.nextLine();
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

# Κυρίως Πρόγραμμα
Αυτός είναι ο πηγαίος κώδικας του προγράμματός :


\
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
import java.io.BufferedReader;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;

class Rout{
	// Global vars
	public static int[] dist = new int[7];
	public static Node[] n = new Node[7];
	public static String input = "n1";

	// Main
	public static void main(String[] args){

		// Initialise Nodes
		for (int i=0; i<=6; i++ ){
			// make nodes
			n[i] = new Node("n" + String.valueOf(i), i);
			// init dist to 999
			dist[i] = 999;
		}
		// Ask user for node
		System.out.println("Select Node :\n");
		for (int i=1; i<=6; i++)
			System.out.println(n[i].name);
		Scanner sc = new Scanner(System.in);
		input = sc.nextLine();

		// READ from file
		System.out.println("------Read file------");//<-
		try(BufferedReader br = new BufferedReader(new FileReader("./graph2.txt"))) {
			String line;
			int i=1;
			// Split Lines into variables
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				String[] arr = line.split(",");
				// Assign near nodes and distances
				n[i].setNn1(returnNode(arr[0]), Integer.parseInt(arr[1]));
				n[i].setNn2(returnNode(arr[2]), Integer.parseInt(arr[3]));
				n[i].setNn3(returnNode(arr[4]), Integer.parseInt(arr[5]));
				n[i].setNn4(returnNode(arr[6]), Integer.parseInt(arr[7]));
				i++;
			}
		}catch(Exception e){
			e.printStackTrace();//<-
		}


		// Distance of first node to itself
		dist[returnNode(input).index] = 0;
		System.out.println("\n\n----Routing Table----\nn1\tn2\tn3\tn4\tn5\tn6");//<-
		// Routing Algorythm
		fnn(returnNode(input), 0);

		System.out.println("\n\n-------Results-------");//<-
		// Print all distances
		for(int i=1; i<=6 ; i++){
			System.out.println(input + " -> n" + i + "   [" + dist[i] +"]");
		}


	}	// End of main

	// Find a node by its name
	public static Node returnNode(String name){
		for(Node node:n){
			if (node.name.equals(name)){
				return node;
			}
		}
		return null;
	}

	// Routing algorythm
	public static void fnn(Node ni, int travel){
		Node[] node = ni.getNodes();
		for (int i=1; i<= 6; i++){
			System.out.printf(dist[i] + "\t");//<-
		}
		System.out.println("");//<-

		Node less = null;
		int lessv = 999;
		// Find closest distance
		for (int i=1; i<= 4 ; i++){
			if( node[i] != null ){
				int nodeIndex = node[i].index;
				int nodeDist = ni.getNd(i) + travel;
				if( dist[node[i].index] > nodeDist){
					dist[node[i].index] = nodeDist;
				}
				if(( !node[i].passed ) & ( lessv > ni.getNd(i))){
					less = node[i];
					lessv = ni.getNd(i);
				}
			}
		}

		if ( less != null ){
			ni.pass();
			fnn( less, lessv + travel );
		}
	}
}	// End of class

```

\pagebreak

# Αποτελέσματα

Η έξοδος του προγράμματος είναι αρχικά το αρχείο που διαβάστηκε.

\
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```C
						------Read file------
						n2,8,n4,1,null,0,null,0
						n1,8,n4,5,n5,1,n3,4
						n2,4,n4,1,n5,1,n6,1
						n1,1,n2,5,n3,1,n5,1
						n4,1,n2,1,n3,1,n6,4
						n5,4,n3,1,null,0,null,0
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Έπειτα είναι ο πίνακας δρομολόγησης :


\
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```C
						----Routing Table----
					n1      n2      n3      n4      n5      n6
					0       999     999     999     999     999
					0       8       999     1       999     999
					0       6       2       1       2       999
					0       6       2       1       2       3
					0       4       2       1       2       3

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και τέλος συγκεκριμένα τα αποτελέσματα για κάθε κόμβο.

\
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```C
						-------Results-------
						   n1 -> n1   [0]
						   n1 -> n2   [4]
						   n1 -> n3   [2]
						   n1 -> n4   [1]
						   n1 -> n5   [2]
						   n1 -> n6   [3]
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
