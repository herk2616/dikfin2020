import java.io.BufferedReader;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;

class Rout{
	// Global vars
	public static int[] dist = new int[7];
	public static Node[] n = new Node[7];
	public static String input = "n1";

	// Main
	public static void main(String[] args){

		// Initialise Nodes
		for (int i=0; i<=6; i++ ){
			// make nodes
			n[i] = new Node("n" + String.valueOf(i), i);
			// init dist to 999
			dist[i] = 999;
		}
		// Ask user for node
		System.out.println("Select Node :\n");
		for (int i=1; i<=6; i++)
			System.out.println(n[i].name);
		Scanner sc = new Scanner(System.in);
		input = sc.nextLine();

		// READ from file
		System.out.println("------Read file------");//<-
		try(BufferedReader br = new BufferedReader(new FileReader("./graph.txt"))) {
			String line;
			int i=1;
			// Split Lines into variables
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				String[] arr = line.split(",");
				// Assign near nodes and distances
				n[i].setNn1(returnNode(arr[0]), Integer.parseInt(arr[1]));
				n[i].setNn2(returnNode(arr[2]), Integer.parseInt(arr[3]));
				n[i].setNn3(returnNode(arr[4]), Integer.parseInt(arr[5]));
				n[i].setNn4(returnNode(arr[6]), Integer.parseInt(arr[7]));
				i++;
			}
		}catch(Exception e){
			e.printStackTrace();//<-
		}


		// Distance of first node to itself
		dist[returnNode(input).index] = 0;
		System.out.println("\n\n----Routing Table----\nn1\tn2\tn3\tn4\tn5\tn6");//<-
		// Routing Algorythm
		fnn(returnNode(input), 0);

		System.out.println("\n\n-------Results-------");//<-
		// Print all distances
		for(int i=1; i<=6 ; i++){
			System.out.println(input + " -> n" + i + "   [" + dist[i] +"]");
		}


	}	// End of main

	// Find a node by its name
	public static Node returnNode(String name){
		for(Node node:n){
			if (node.name.equals(name)){
				return node;
			}
		}
		return null;
	}

	// Routing algorythm
	public static void fnn(Node ni, int travel){
		Node[] node = ni.getNodes();
		for (int i=1; i<= 6; i++){
			System.out.printf(dist[i] + "\t");//<-
		}
		System.out.println("");//<-

		Node less = null;
		int lessv = 999;
		// Find closest distance
		for (int i=1; i<= 4 ; i++){
			if( node[i] != null ){
				int nodeIndex = node[i].index;
				int nodeDist = ni.getNd(i) + travel;
				if( dist[node[i].index] > nodeDist){
					dist[node[i].index] = nodeDist;
				}
				if(( !node[i].passed ) & ( lessv > ni.getNd(i))){
					less = node[i];
					lessv = ni.getNd(i);
				}
			}
		}

		if ( less != null ){
			ni.pass();
			fnn( less, lessv + travel );
		}
	}
}	// End of class

